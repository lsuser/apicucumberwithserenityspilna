package api.definitions;

import api.steps.GenericSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GenericDefinitions {

    @Steps
    GenericSteps genericSteps;

    private Response response;

    //Method used for initializing the Map with http query parameters (key,value) pairs
    @Given("the client uses the following url parameters:")
    public void theFollowingUrlParameters(Map<String, String> urlParameters)
    {
        genericSteps.setDecorateUrl(urlParameters);
    }

    // Method that checks a jsonPath from response has exact value with given string
    @Then("the client using jsonPath {string} should see having {string}")
    public void theClientShouldSeeContainingString(String jsonPath, String expected)
    {
        List<String> result = JsonPath.from(response.asString()).get(jsonPath);
        assertTrue(String.format("Expected - %s but found - %s ",expected, result) ,result.contains(expected));
    }

    // Method that checks a jsonPath from response hasn't exact value with given string
    @Then("the client using jsonPath {string} should NOT see having {string}")
    public void theClientShouldNotSeeContainingString(String jsonPath, String expected)
    {
        try
        {
            List<String> result = JsonPath.from(response.asString()).get(jsonPath);
            assertFalse(String.format("Not expected but found - %s ",expected) ,result.contains(expected));
        }
        catch (Exception e){
            assertTrue(e.toString().contains("NullPointerException"));
        }
    }

    //Same as "the client calls {string} endpoint" method but also checks that HTTP response code is 200
    @When("the client calls {string} endpoint with success")
    public void theUserCallsEndpointWithSuccess(String webServiceEndpoint)
    {
        theUserCallsEndpoint(webServiceEndpoint);
        theClientShouldReceiveHttpResponseCode(200);
    }

    //Method that check the response object that has status code XXX (type int)
    @Then("the client should receive an HTTP {int} response code")
    public void theClientShouldReceiveHttpResponseCode(int httpCode) {
        assertThat(response.getStatusCode(), equalTo(httpCode));
    }

    //Method that calls a web service endpoint configured in the enum class WebServiceEndpoints.
    // The string used needs to be defines in the enum class
    @When("the client calls {string} endpoint")
    public void theUserCallsEndpoint(String webServiceEndpoint) {
        this.response = genericSteps.callEndpoint(webServiceEndpoint);
    }

}
