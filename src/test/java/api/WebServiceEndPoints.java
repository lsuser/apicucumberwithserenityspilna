package api;

public enum WebServiceEndPoints {
    SEARCH_BOOKS("https://www.googleapis.com/books/v1/volumes", "GET");

    private final String url;
    private final String method;

    WebServiceEndPoints(String url, String method) {
        this.url = url;
        this.method = method;
    }

    public String getUrl() {
        return url;
    }
    public String getMethod() {return method;}
}
