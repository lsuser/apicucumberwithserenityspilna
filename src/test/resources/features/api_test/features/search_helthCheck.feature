Feature: Basic checks for '/books/v1/volumes' healthCheck

  @automated @smoke
  Scenario: Should receive 200 response code by search - search+terms
    Given the client uses the following url parameters:
      | q | search+terms |
    When the client calls 'SEARCH_BOOKS' endpoint with success

