Feature: Basic checks for '/books/v1/volumes' with negative cases

  @automated @smoke
  Scenario: Should receive 200 response code
    by search with parameter 'false6543'
    Given the client uses the following url parameters:
      | q | false6543 |
    When the client calls 'SEARCH_BOOKS' endpoint with success

  @automated @regression
  Scenario: Should receive 200 response code
  by search with parameter 'false6543'
  and not get data for a publisher
    Given the client uses the following url parameters:
      | q | false6543 |
    When the client calls 'SEARCH_BOOKS' endpoint with success
    Then the client using jsonPath 'items.volumeInfo.publisher' should NOT see having 'Greenwood Publishing Group'


  @automated @regression
  Scenario: Should receive 400 response code by search - ''
    Given the client uses the following url parameters:
      | q |  |
    When the client calls 'SEARCH_BOOKS' endpoint
    Then the client should receive an HTTP 400 response code
