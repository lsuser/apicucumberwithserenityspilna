Feature: Basic checks for '/books/v1/volumes' by 'search+terms' and check publisher

  @automated @regression
  Scenario: Use parameter search+terms
  should receive 200 response code
  and get correct publisher 'Greenwood Publishing Group'
    Given the client uses the following url parameters:
      | q | search+terms |
    When the client calls 'SEARCH_BOOKS' endpoint
    Then the client using jsonPath 'items.volumeInfo.publisher' should see having 'Greenwood Publishing Group'

  @automated @smoke
  Scenario: Use parameter search+terms
  should receive 200 response code
  and get correct publisher 'Elsevier Science Limited'
    Given the client uses the following url parameters:
      | q | search+terms |
    When the client calls 'SEARCH_BOOKS' endpoint
    Then the client using jsonPath 'items.volumeInfo.publisher' should see having 'Elsevier Science Limited'

