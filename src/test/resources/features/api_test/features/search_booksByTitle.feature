Feature: Basic checks for /books/v1/volumes by title and check title
  
  @automated @smoke
  Scenario: Use parameter search+terms
  should receive 200 response code
  and get correct title = 'Dividend Options'
    Given the client uses the following url parameters:
      | q | Dividend |
    When the client calls 'SEARCH_BOOKS' endpoint
    Then the client using jsonPath 'items.volumeInfo.title' should see having 'Dividend Options'

