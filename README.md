##### Automated 
- Based on Serenity+Cucumber4
- Capabilities
  - run predefined suites (smoke, regression, automated, complete feature)
  - do REST API (GET)calls to predefined endpoints
  - check the response object that has status code XXX (type int)
  - check a jsonPath from response has exact value with given string
  - check a jsonPath from response hasn't exact value with given string
  
- Structure
  - src/test/java/api - Test runners and supporting code
  - src/test/resources/features - Feature files
  
- How to run:
  - Prerequisites: maven3, java8 or greater
  - JUnit:
    - go to **src/test/java/api/** and run class **CucumberTestSuite.java** (will run all testcases with @automated tag by default)
    - you can modify the tags you want to run from @CucumberOptions inside the class
  - Maven:
    - run command from base project: **mvn clean verify** (will run all testcases with @automated tag by default)
    - if you want to run different tags: ** mvn clean verify -Dcucumber.options="--tags @test"**
    for example:
    mvn clean verify -Dcucumber.options="--tags @regression
    mvn clean verify -Dcucumber.options="--tags @smoke
    
    - html report is generated when running previous commands - open target/site/serenity/index.html after run